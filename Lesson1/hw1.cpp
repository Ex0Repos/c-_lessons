#include <iostream>
// #include <cstdlib>	// Just in case C headers are needed

using namespace std;

int main()
{
	cout << "Hello\nWorld!" << endl << endl;
	/* Alternate solutions
		cout << "Hello" << endl << "World!" << endl;
		cout << "Hello\nWorld!\n" << endl;
		cout << "Hello" << "\n" << "World!" << "\n";
		...And any combinations
	 */

	cout << "My name" << endl << "is Adheesh." << endl << endl;
	cout << "I am" << endl << "123423 years old." << endl;

	system("pause");
	return 0;	// A feature of C++, as compared to C, is that you do not need to put this statement but,
				// you should just for consistency in your code
}