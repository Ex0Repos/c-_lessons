#include <iostream>

using namespace std;

int main()
{
	int a = 10;			// Integer variable with direct assignment using '='
	char b('b');		// Char(acter) variable with direct assignment using constructor-style
	float c{ 1.f };		// Float(ing-point) variable with direct assignment using initializer lists
	double d;			// Double variable with late assignment
	d = 100.f;			// Assigning a value to 'd'

	cout << "int : " << a << endl;
	cout << "char : " << b << endl;
	cout << "float : " << c << endl;
	cout << "double : " << d << endl << endl;

	a = b * 20;			// Characters also have an integer value as per ASCII
	b = a / 100;		// --------------- "" -------------------------------
	c = d / 4;			// This is a comment.
	d = b * a + c;		// This is another comment.

	cout << "int : " << a << endl;
	cout << "char : " << b << endl;
	cout << "float : " << c << endl;
	cout << "double : " << d << endl << endl;

	system("pause");
	return 0;
}