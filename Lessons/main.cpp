#include <iostream>
#include <cstdlib>		// I never noticed, but I do not think you need to include C headers, but you should to be safe

using namespace std;

int main()
{
	cout << "Hello World!" << endl;

	system("pause");	// This makes the program stay open to see output, without it, the window would quickly popup and close
	// cin.get()		// You can also use this instead, but instead of pressing any key, you have to press <ENTER>
	return 0;
}